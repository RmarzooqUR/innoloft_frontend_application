import React from 'react';
import './App.css';
import Tabs from './components/Tabs';
import styles from './App.module.css'


function App() {
  return (
    <div>
      <div>
        <NavHeader />
      </div>
      <div className={styles.Content}>
        <Sidebar />
        <Tabs />
      </div>
    </div>
  );
}

const NavHeader = () => {
  return (
      <div className={styles.NavHeader} ></div>
    )
}

const Sidebar = () =>{
  return (
      <div className={styles.Sidebar}>
        <div className={styles.SidebarContent}></div>
      </div>
    )
}
export default App;
