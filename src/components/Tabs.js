import React, { useState } from 'react';
import Tab1 from './Tab1';
import Tab2 from './Tab2';
import styles from './css/Tabs.module.css'

export default function Tabs(){
	const [visible, setVisible] = useState({
		Tab1:true,
		Tab2:false
	});

	const handleClick = (newVisibility) => {
		setVisible(newVisibility);
	}
	return (
			<div className={styles.Tabs}>
				<div className={styles.TabHeaders}>
					<div 
						className={styles.TabHeader}
						key={0} onClick={()=>handleClick({Tab1:true, Tab2:false})}>
							<h3>Change Credentials</h3>
					</div>
					<div 
						className={styles.TabHeader}
						key={1} onClick={()=>handleClick({Tab1:false, Tab2:true})}>
							<h3>Change Details</h3>
					</div>
				</div>
				<div>
					{visible.Tab1 && (<Tab1 />)}
					{visible.Tab2 && (<Tab2 />)}
				</div>
			</div>
		)
}