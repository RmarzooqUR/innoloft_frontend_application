import React, { useState } from 'react';

export default function Tab2(){
	const handleClick = (e) =>{
		fetch('https://jsonplaceholder.typicode.com/posts', {
			method:'POST',
			body:'userdata'
		})
		.then(
			(resp)=>console.log(resp),
			(err)=>console.log(err)
			)
		e.preventDefault()
	}
	return (

		<form onSubmit={handleClick}>
			<input type='text' placeholder='First Name' />
			<input type='text' placeholder='Last Name' />
			<input type='text' placeholder='Street Name' />
			<input type='text' placeholder='House No' />
			<input type='text' placeholder='Postal Code' />
			<select name='Country' id='country'>
				<option value='Germany'>Germany</option>
				<option value='Austria'>Austria</option>
				<option value='Switzerland'>Switzerland</option>
			</select>
			<input type='submit' value='Update'/>
		</form>
		)
}