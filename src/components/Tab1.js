import React, { useState } from 'react';

export default function Tab1({ active }){
	const [helperEmail,setHelperEmail] = useState({
		show:false,
		regex:/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/,
		msg:'email should be of the form example@example.com'
	})

	const handleEmail = (event) =>{
		event.target.value?(
			event.target.value.match(helperEmail.regex)?
				setHelperEmail((prev)=>({...prev, show:false}))
				:setHelperEmail((prev)=>({...prev, show:true}))
		):setHelperEmail((prev)=>({...prev, show:false}))
	}



// ==========================================================================================



	const [helperPassword, setHelperPassword] = useState({
		show:false,
		password:null,
		msg:[]
	})

	const passwordRegex = [
		/^[a-zA-Z0-9.!#$%&@'*+/=?^_`{|}~-]*[A-Z]+[a-zA-Z0-9.!@#$%&'*+/=?^_`{|}~-]*$/,
		/^[a-zA-Z0-9.!#$%&'@*+/=?^_`{|}~-]*[a-z]+[a-zA-Z0-9.!#@$%&'*+/=?^_`{|}~-]*$/,
		/^[a-zA-Z0-9.!#$%&'*@+/=?^_`{|}~-]*[0-9]+[a-zA-Z0-9.!#$@%&'*+/=?^_`{|}~-]*$/,
		/^[a-zA-Z0-9.!#$%&'*+@/=?^_`{|}~-]*[.!#$%&@'*+/=?^_`{|}~-]+[a-zA-Z0-9.!@#$%&'*+/=?^_`{|}~-]*$/
	];
	const passwordMsg = [
		'Your password must have at least one uppercase letter',
		'Your password must have at least one lowercase letter',
		'Your password must have at least a number',
		'Your password must have at least a special character',
	];

	const handlePassword = (event) =>{
		const val = event.target.value;
		if(val){
			const msgs = passwordRegex.map((item, index)=>{
				return val.match(item)?
					null:passwordMsg[index]
				}
			)
			console.log(msgs)
			msgs.forEach((item)=>{
				item?setHelperPassword((prev)=>(
					{
						...prev, 
						show:true, 
						msg:msgs,
						password:val
					}))
					:setHelperPassword((prev)=>(
						{
							...prev,
							show:false,
							password:val
						}))
			})
		}else
			setHelperPassword((prev)=>({...prev, show:false}))
	};


// =============================================================================================



	const [helperReenterPass, setHelperReneterPass] = useState({
		show:false,
		msg:'Passwords do not match'
	})

	const handleReenter = (event) =>{
		event.target.value?(
				event.target.value == helperPassword.password?(
						setHelperReneterPass((prev)=>({...prev, show:false}))
					):setHelperReneterPass((prev)=>({...prev, show:true}))
			):setHelperReneterPass((prev)=>({...prev, show:false}))
	}




	const handleSubmit = (event) =>{
		fetch('https://jsonplaceholder.typicode.com/posts', {
			method:'POST',
			body:'userdata'
		})
		.then(
			(resp)=>console.log(resp),
			(err)=>console.log(err)
		)
		event.preventDefault()
	}
	return (
			<form onSubmit={handleSubmit}>
				<input type='text' placeholder='Email' onChange={handleEmail} />
				{helperEmail.show && <p>{helperEmail.msg}</p>}
				
				<input type='password' placeholder='Password' onChange={handlePassword}/>
				{helperPassword.show && 
					<ul>{helperPassword.msg.map((item)=>
							item && <li>{item}</li>
						)}
					</ul>}
				
				<input type='password' placeholder='Re-enter Password' onChange={handleReenter}/>
				{helperReenterPass.show && <p>{helperReenterPass.msg}</p>}
				<input type='submit' value="update"/>
			</form>
		)
}